class User < ApplicationRecord

  validates :fullname, presence: { message: "The name is required" }
  validates :title, presence: { message: "The title is required" }

  has_attached_file :image
  validates_attachment_content_type :image, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"]

  has_many :articles

end
