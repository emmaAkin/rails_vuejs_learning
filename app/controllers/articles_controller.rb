class ArticlesController < ApplicationController

  def create
    @article = Article.new(article_params)
    @article.user = User.first
    if @article.save
      render json: ['article' => @article, 'message' => 'article created']
    else
      render json: @article.errors, status: :unprocessable_entity
    end
  end

  def update
    @article = Article.find(params[:id])
    if @article.update(article_params)
      render json: ['article' => @article, 'message' => 'article updated']
    else
      render json: @article.errors, status: :unprocessable_entity
    end
  end

  def search
    if params[:search_text]
      render json: @articles = Article.where('title LIKE ? or text LIKE ?', "%#{params[:search_text]}%", "%#{params[:search_text]}%").order("created_at DESC")
    end
  end

  private

  def article_params
    params.permit(:title, :text, :user_id)
  end

end
