class UsersController < ApplicationController
  before_action :set_user, only: [:update]

  def update
    if @user.update(user_params)
      render json: ['user' => @user, 'message' => 'user updated']
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  private

  def set_user
    @user = User.find(params[:id])
  end

  def user_params
    params.permit(:fullname, :title, :image)
  end

end
