class PagesController < ApplicationController
  def home
    @user = User.find(1)
    @articles = User.find(1).articles.order(id: :desc)
  end

  def search
    @articles = User.find(1).articles.order(id: :desc)
  end
end
