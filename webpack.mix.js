const { mix } = require('laravel-mix');

mix.js('resources/assets/javascripts/app.js', 'app/assets/javascripts/app.js')
    .sass('resources/assets/stylesheets/app.scss', 'app/assets/stylesheets/app.css')
    .options({processCssUrls: false});