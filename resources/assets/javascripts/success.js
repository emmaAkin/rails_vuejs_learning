class Success {
    constructor() {
        this.message = null;
    }
    record(message) {
        this.message = message;
    }
    clear() {
        if (this.message) {
            this.message = null;
        }
    }
}
export { Success };