# README

- to install this app:
    - bundle install
    - rails db:migrate
    - rails db:seed
    - run yarn run watch or npm run watch to compile the js and scss (or yarn run dev or yarn run production).
    - rails s